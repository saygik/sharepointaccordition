Vue.component('acdepartment', {
    template: '#acc-list-template',
    props: {
        item: Object
    },
    data: function(){
        return{
            itemvisible: this._props.item.visible,
            itemEmpty:false,
            contactsArray: this._props.item.filterFIO
        }
    },
    methods:{
        ContactClass: function(item){
                      return "profile-splitter"
             },
        item_click: function(){
            var self = this;
            if(this.itemvisible){
                this.itemvisible=false;
            }else {
                if (this._props.item.filterFIO.length>0){
                    this.itemvisible=true;
                    return;
                }
                var idd = self._props.item.idd;
                if (this._props.item.idd > 0) {
                    var url = "http://nod2.brnv.rw/_api/web/lists(guid'4b916f60-2c1e-4a0e-aad9-3f013e0187dd')/items?$select=Title,dolg,telefon,num,email,sip,comment,department/Title,department/ID&$expand=department&$orderby=num&$filter=department/ID eq ";
                    url += idd;
                    $.ajax({
                        crossDomain: true,
                        contentType: "application/json",
                        dataType: "json",
                        type: "GET",
                        url: url,
                        headers: {"accept": "application/json;odata=verbose"},
                        success: function (data) {
                                var resArray = [];
                                for (var i in data.d.results) {
                                    resArray.push( data.d.results[i]);
                                }
                                self.loading = false;
                                if(resArray.length>0){

                                   self.contactsArray = resArray;
                                   self.itemvisible = true;
                                } else{
                                    self.itemEmpty=true;
                                    setTimeout(function () { self.itemEmpty=false;},50);
                                    setTimeout(function () { self.itemEmpty=true;},200);
                                    setTimeout(function () { self.itemEmpty=false;},250);

                                }
                        },
                        error: function (request, status, error) {
                            self.loading = false;
                            self.itemvisible = false;
                            console.log(request.responseText)
                        }

                    });
                }
            }
        }
    },
    computed: {
        computedClass: function () {
            return(this.itemvisible ) ? 'cbp-ntopen' : '';
        },
        departmetClasses: function () {
            var cClass='cbp-nttrigger';
            cClass+= (this.itemEmpty ) ? ' cbp-ntaccordion-empty' : '';
            return cClass;
        }
    },
    watch : {
        item: function () {
            this.itemvisible=this._props.item.visible;
            if (this._props.item.filterFIO.length>0){
                this.contactsArray= this._props.item.filterFIO;
            }

        }
    }
});

Vue.component('acitem', {
    template: '#acc-main-list-template',
    props: {
        item: Object
    },
    data: function () {
        return {
            itemvisible: this._props.item.visible
        }
    },
    methods:{
        item_click: function(){
         this.itemvisible=!this.itemvisible;
        }
    },
    watch : {
        item: function () {
         this.itemvisible=this._props.item.visible;
        }
    }
});
new Vue({
    el: '#accordition-list',
    data: {
        filterDepartment: "",
        filterFIO: "",
        answer: "Поиск по ФИО или телефону...",
          loading: false,
        nod2items:[],
        filterFIOArray:[]
    },
    watch:{
        filterFIO:function () {
            if(this.filterFIO.length>2){
                this.filterFIOArray=[]
            }else {
                self.loading = false;
            }
            this.answer = "Ожидаю, когда вы закончите печатать...";
//            this.filterDepartment="";
            this.getFilterFIO();
        }
    },
    methods:{
           getFilterFIO: _.debounce(function() {
            var self=this;
            if(self.filterFIO.length>2){
                self.loading = true;
                this.answer = 'Выполнение запроса к серверу...';

                var url=encodeURI("http://nod2.brnv.rw/_api/web/lists(guid'4b916f60-2c1e-4a0e-aad9-3f013e0187dd')/items"+
                    "?$select=Title,dolg,telefon,num,email,sip,comment,department/Title,department/ID"+
                    "&$expand=department&$orderby=num"+
                    "&$filter=substringof('"+self.filterFIO+"',Title) or substringof('"+self.filterFIO+"',telefon)");
                $.ajax({
                    crossDomain: true,
                    contentType: "application/json",
                    dataType: "json",
                    type: "GET",
                    url: url,
                    headers: {"accept": "application/json;odata=verbose"},
                    success: function (data) {
                        self.answer = "Результат получен.";
                        var tempitemsFIO=[];
                        var nkey;
                        var depObject={};
                        var findRegexp=new RegExp(self.filterFIO, 'ig');
                        for (var i in data.d.results)
                        {
                            nkey=data.d.results[i].department.ID;
                            if(!isNaN(nkey))
                            {
                                depObject={"idd":nkey,"Title":data.d.results[i].Title.replace(findRegexp,'<font color=red>$&</font>'),"email":data.d.results[i].email,"sip":data.d.results[i].sip,
                                           "dolg":data.d.results[i].dolg,"comment":data.d.results[i].comment,"telefon":(data.d.results[i].telefon || "").replace(findRegexp,'<font color=red>$&</font>')};
                                tempitemsFIO.push(depObject);
                            }
                        }
                        self.loading = false;
                        self.filterFIOArray=tempitemsFIO;
                        setTimeout(function () {
                            self.answer = "Поиск по ФИО или телефону...";
                        }, 2000);
                    },
                    error: function (request, status, error) {
                        self.loading = false;
                        self.answer= "Поиск по ФИО или телефону..."
                        console.log(request.responseText)
                    }
                });
            }
            else{
                this.answer = "Поиск по ФИО или телефону...";
            }
        },1500)

    },
    computed:{
        items: function() {
            var self = this;
            var IsSartingFIOFilter=self.filterFIO.length>2;
            if(IsSartingFIOFilter && self.filterFIOArray.length===0) {
                self.loading = true;
                this.emptyFilter=false;
                return []
            }
            var clone_nod2items=JSON.parse(JSON.stringify(self.nod2items)).splice(1); /* Создание клона массива для сохранения в nod2items исходных значений*/
            var VRegExp = new RegExp(/^[ ]+/g);                                       /* Выражение для удаление первых пробелов*/
            var findRegexp=new RegExp(self.filterDepartment, 'ig');
            var result = [];
            var resultDepartments = [];
            if(self.filterDepartment.length>2){                                       /* Начинать фильтровать если в поле поиска более 2 символов */
                result = clone_nod2items.filter(function (item) {
                var IsItemNameNotFiltred=(item.name.toLowerCase().indexOf(self.filterDepartment.replace(VRegExp,'').toLowerCase()) !== -1);
                    item.name=item.name.replace(findRegexp,'<font color=red>$&</font>')
                var IsItemDepartmentNotFiltred=true;
                  /* Если в поле поиска по ФИО более 2 символов - проверка есть ли в этом предприятии отфильтрованные пользователи*/
                  if(IsSartingFIOFilter){
                      resultDepartments=item.departments.filter(function (item) {
                          return (self.filterFIOArray.reduce(function(result,elem) {
                              if(elem.idd===item.idd){
                                  item.filterFIO.push(elem)
                                  item.visible=true;
                                  return true || result
                              }
                              return false || result
                          },false));
                      })
                      item.visible=resultDepartments.length>0;
                  }
                  else{
                      resultDepartments=item.departments
                  }

                if(!IsItemNameNotFiltred){
                    resultDepartments=resultDepartments.filter(function (item) {
                        var isfullDepName=false;
                        var isdepTitle=false;
                        if(item.depTitle){
                            isdepTitle=(item.depTitle.toLowerCase().indexOf(self.filterDepartment.replace(VRegExp,'').toLowerCase()) !== -1)
                            item.depTitle=item.depTitle.replace(findRegexp,'<font color=red>$&</font>')
                        }
                        if(item.fullDepName){
                            isfullDepName=(item.fullDepName.toLowerCase().indexOf(self.filterDepartment.replace(VRegExp,'').toLowerCase()) !== -1)
                            item.fullDepName=item.fullDepName.replace(findRegexp,'<font color=red>$&</font>')
                        }
                        return (isdepTitle || isfullDepName);
                    })
                }
                  if (resultDepartments.length>0){
                      item.visible=true;
                      IsItemDepartmentNotFiltred=false;
                      item.departments=resultDepartments;
                  }
                return IsItemNameNotFiltred || !IsItemDepartmentNotFiltred;
              })
            }
            else{
                if(IsSartingFIOFilter){
                    result = clone_nod2items.filter(function (item) {
                        resultDepartments=item.departments.filter(function (item) {
                            return (self.filterFIOArray.reduce(function(result,elem) {
                                if(elem.idd===item.idd){
                                    item.filterFIO.push(elem)
                                    item.visible=true;
                                    return true || result
                                }
                                return false || result
                            },false));
                        })
                        if (resultDepartments.length>0){
                            item.visible=true;
                            item.departments=resultDepartments;
                            return true;
                        }
                        return false;
                    })
                }
                else {
                    result = clone_nod2items;
                    self.loading = false;
                }
            }
            return result
        }
    },
    mounted: function () {
        var self = this;
      self.loading = true;
        var requestUri = "http://nod2.brnv.rw/_api/web/lists(guid'ded1cea5-c767-4828-9ad0-4462484c2925')/items?$select=ID,Title,fulldepname,num,depnum,pred/Title,pred/ID&$expand=pred&$orderby=num,depnum";
        var requestHeaders = { "accept": "application/json;odata=verbose" };
//        document.domain = 'nod2.brnv.rw';
        $.support.cors = true;
        $.ajax({
            crossDomain: true,
            contentType: "application/json",
            dataType : "json",
            type: "GET",
            url: requestUri,
            headers: requestHeaders,
            success: function(data){
                setTimeout(function(){
                    var yy=[];
                    var nkey;
                    var depObject={};
                    for (var i in data.d.results)
                    {
                        nkey=data.d.results[i].pred.ID;
                        if(!isNaN(nkey))
                        {
                            depObject={"idd":data.d.results[i].ID,"depNum":data.d.results[i].depnum,"depTitle":data.d.results[i].Title,"fullDepName":data.d.results[i].fulldepname,"visible":false,"filterFIO":[]}
                            if(!(nkey in yy)){
                                yy[nkey]={"name":data.d.results[i].pred.Title,"departments":[depObject],"visible":false};
                            }
                            else{
                                yy[nkey].departments.push(depObject);
                            }
                            //do some thing if it's a number
                        }
                    }
                    self.nod2items=yy;
                    self.loading = false;

                }, 1000);
            },
            error: function(request, status, error){
                self.loading = false;
                console.log(request.responseText)
            }
        });
    }
})